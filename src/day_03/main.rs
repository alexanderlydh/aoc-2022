use crate::utils::file::read_lines;

pub fn run() {
    let content = read_lines("resources/day3.txt");
    println!("Day 03:");
    first(&content);
    second(&content);
    println!("");
}

fn first(content: &Vec<String>) -> i32 {
    let score = content
        .iter()
        .map(split_in_two)
        .map(find_overlap2)
        .map(calc_value)
        .sum();
    println!("{:?}", score);
    score
}

#[test]
fn test_first() {
    let content = read_lines("resources/day3_test.txt");
    let result = first(&content);

    assert_eq!(157, result)
}

fn second(content: &Vec<String>) -> i32 {
    let score = content.chunks(3).map(find_overlap3).map(calc_value).sum();
    println!("{:?}", score);
    return score;
}

#[test]
fn test_second() {
    let content = read_lines("resources/day3_test.txt");
    let result = second(&content);

    assert_eq!(70, result)
}

fn split_in_two(items: &String) -> (&str, &str) {
    let mid = items.chars().count() / 2;
    items.split_at(mid)
}

fn find_overlap2((a, b): (&str, &str)) -> char {
    for c in a.chars() {
        if b.contains(c) {
            return c;
        }
    }
    panic!("No intersection")
}

fn find_overlap3(items: &[String]) -> char {
    for c in items[0].chars() {
        if items[1].contains(c) && items[2].contains(c) {
            return c;
        }
    }
    panic!("No intersection")
}

fn calc_value(c: char) -> i32 {
    let offset = if c.is_lowercase() {
        'a' as u32 - 1
    } else {
        'A' as u32 - 27
    };

    let value = c as u32 - offset;
    value.try_into().unwrap()
}
