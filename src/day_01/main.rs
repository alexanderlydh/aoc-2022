use crate::utils::file::read_lines;

pub fn run() {
    let content = read_lines("resources/day1.txt");
    let parsed_content = parse_content(&content);
    println!("Day 01:");
    part_one(&parsed_content);
    part_two(&parsed_content);
    println!("")
}

fn part_one(parsed_content: &Vec<usize>) {
    println!("{:?}", parsed_content.into_iter().max());
}

fn part_two(parsed_content: &Vec<usize>) {
    let mut local_content = parsed_content.clone();
    local_content.sort();
    let three = local_content.into_iter().rev().take(3).sum::<usize>();
    println!("{:?}", three);
}

fn parse_content(content: &Vec<String>) -> Vec<usize> {
    content
        .split(|e| e.is_empty())
        .map(|foods| {
            foods
                .iter()
                .map(|food| food.parse::<usize>().unwrap())
                .sum::<usize>()
        })
        .collect()
}
