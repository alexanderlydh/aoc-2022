use crate::utils::file::read_lines;
use std::{num::ParseIntError, str::FromStr};

pub fn run() {
    let content = read_lines("resources/day4.txt");
    println!("Day 04:");
    first(&content);
    second(&content);
    println!("");
}

fn first(content: &Vec<String>) -> usize {
    common(content, envelopes)
}

#[test]
fn test_first() {
    let content = read_lines("resources/day4_test.txt");
    let result = first(&content);

    assert_eq!(2, result)
}

fn second(content: &Vec<String>) -> usize {
    common(content, overlaps)
}

#[test]
fn test_second() {
    let content = read_lines("resources/day4_test.txt");
    let result = second(&content);

    assert_eq!(4, result)
}

#[derive(Copy, Clone, Debug)]
struct Range {
    lower: i32,
    upper: i32,
}

impl Range {
    fn envelopes(self: Self, other: Range) -> bool {
        self.lower <= other.lower && other.upper <= self.upper
    }

    fn overlaps(self: Self, other: Range) -> bool {
        self.envelopes(other)
            || (self.lower <= other.lower && other.lower <= self.upper)
            || (self.lower <= other.upper && other.upper <= self.upper)
    }
}

impl FromStr for Range {
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split("-").collect();
        parts[0].parse::<i32>().and_then(|l| {
            parts[1]
                .parse::<i32>()
                .and_then(|u| Ok(Range { lower: l, upper: u }))
        })
    }
    type Err = ParseIntError;
}

fn common(content: &Vec<String>, f: fn((Range, Range)) -> bool) -> usize {
    let score = content
        .iter()
        .map(|assignments| build_ranges(assignments.split(",").collect()))
        .map(f)
        .filter(|o| *o)
        .count();
    println!("{:?}", score);
    score
}

fn envelopes((a, b): (Range, Range)) -> bool {
    a.envelopes(b) || b.envelopes(a)
}

fn overlaps((a, b): (Range, Range)) -> bool {
    a.overlaps(b) || b.overlaps(a)
}

fn build_ranges(ass: Vec<&str>) -> (Range, Range) {
    let a = Range::from_str(ass[0]).unwrap();
    let b = Range::from_str(ass[1]).unwrap();
    (a, b)
}
