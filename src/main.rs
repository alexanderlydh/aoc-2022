mod day_01;
mod day_02;
mod day_03;
mod day_04;
mod day_05;
mod day_06;
mod utils;

fn main() {
    day_01::main::run();
    day_02::main::run();
    day_03::main::run();
    day_04::main::run();
    day_05::main::run();
    day_06::main::run();
}
