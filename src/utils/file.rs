use std::{
    fs::File,
    io::{BufRead, BufReader},
};

pub fn read_lines(filename: &str) -> Vec<String> {
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    reader.lines().filter_map(std::io::Result::ok).collect()
}
