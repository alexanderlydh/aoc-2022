#[allow(dead_code)]
#[derive(Debug)]
pub struct ParseError {
    details: String,
}

#[allow(dead_code)]
impl ParseError {
    pub fn new(msg: &str) -> ParseError {
        ParseError {
            details: msg.to_string(),
        }
    }
}
