use std::{num::ParseIntError, str::FromStr};

use crate::utils::file::read_lines;

pub fn run() {
    let content = read_lines("resources/day5.txt");
    println!("Day 05:");
    common(&content, true);
    common(&content, false);
    println!("");
}

fn common(content: &Vec<String>, reverse: bool) -> String {
    let parts: Vec<&[String]> = content.split(|row| row.is_empty()).collect();
    let crates = parse_crates(parts[0]);
    let instructions = parse_instructions(parts[1]);
    let moved_crates = move_crates(&crates, instructions, reverse);
    let result = moved_crates
        .iter()
        .map(|c| c.first().unwrap())
        .fold("".to_owned(), |cur: String, nxt: &String| cur + nxt);
    println!("{:?}", result);
    return result;
}

fn move_crates(
    crates: &Vec<Vec<String>>,
    instructions: Vec<Instruction>,
    reverse: bool,
) -> Vec<Vec<String>> {
    let mut local_crates = crates.clone();
    for instruction in instructions {
        let mut moving_crates: Vec<String> = local_crates[instruction.from]
            .drain(0..instruction.quantity)
            .collect();
        if reverse {
            moving_crates.reverse();
        }
        moving_crates.extend_from_slice(&local_crates[instruction.to]);
        local_crates[instruction.to] = moving_crates;
    }

    local_crates
}

fn parse_crates(raw_crates: &[String]) -> Vec<Vec<String>> {
    let n = raw_crates.last().unwrap().chars().count() / 4 + 1;
    let mut crates: Vec<Vec<String>> = vec![Vec::new(); n];
    for raw_crate_row in raw_crates.split_last().unwrap().1 {
        let split_row = raw_crate_row
            .chars()
            .collect::<Vec<char>>()
            .chunks(4)
            .map(|c| c.iter().collect::<String>())
            .collect::<Vec<String>>();

        for (i, element) in split_row.iter().enumerate() {
            let trimmed_element = element.trim_end();
            if trimmed_element.is_empty() {
                continue;
            }
            let clean_element = trimmed_element
                .strip_prefix("[")
                .unwrap()
                .strip_suffix("]")
                .unwrap()
                .to_string();
            crates[i].push(clean_element.clone())
        }
    }
    crates
}

#[derive(Debug)]
struct Instruction {
    quantity: usize,
    from: usize,
    to: usize,
}

impl FromStr for Instruction {
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(" ").collect();
        let quantity = parts[1].parse().unwrap();
        let from: usize = parts[3].parse().unwrap();
        let to: usize = parts[5].parse().unwrap();

        Ok(Instruction {
            quantity,
            from: from - 1,
            to: to - 1,
        })
    }
    type Err = ParseIntError;
}

fn parse_instructions(raw_instructions: &[String]) -> Vec<Instruction> {
    raw_instructions
        .iter()
        .map(|i| Instruction::from_str(i).unwrap())
        .collect::<Vec<Instruction>>()
}

#[test]
fn test_first() {
    let content = read_lines("resources/day5_test.txt");
    let result = common(&content, true);

    assert_eq!("CMZ", result)
}

#[test]
fn test_second() {
    let content = read_lines("resources/day5_test.txt");
    let result = common(&content, false);

    assert_eq!("MCD", result)
}
