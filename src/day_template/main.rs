use crate::utils::file::read_lines;

pub fn run() {
    let content = read_lines("resources/day4.txt");
    println!("Day 04:");
    first(&content);
    second(&content);
    println!("");
}

fn first(content: &Vec<String>) -> i32 {
    return -1;
}

#[test]
fn test_first() {
    let content = read_lines("resources/day4_test.txt");
    let result = first(&content);

    assert_eq!(157, result)
}

fn second(content: &Vec<String>) -> i32 {
    return -1;
}

#[test]
fn test_second() {
    let content = read_lines("resources/day4_test.txt");
    let result = second(&content);

    assert_eq!(70, result)
}
