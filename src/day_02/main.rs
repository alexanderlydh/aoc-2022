use crate::utils::file::read_lines;

const ROCKSCORE: i32 = 1;
const PAPERSCORE: i32 = 2;
const SCISSORSCORE: i32 = 3;

#[derive(Debug, Clone, Copy)]
enum Moves {
    Rock,
    Paper,
    Scissor,
}

pub fn run() {
    let content = read_lines("resources/day2.txt");
    println!("Day 02:");
    first(&content);
    second(&content);
    println!("");
}

fn first(content: &Vec<String>) {
    let scores: Vec<i32> = content
        .into_iter()
        .map(|round| handle_round_first(round.split(' ').collect()))
        .collect();
    println!("{:?}", scores.into_iter().sum::<i32>());
}

fn second(content: &Vec<String>) {
    let scores: Vec<i32> = content
        .into_iter()
        .map(|round| handle_round_second(round.split(' ').collect()))
        .collect();
    println!("{:?}", scores.into_iter().sum::<i32>());
}

fn handle_round_second(moves: Vec<&str>) -> i32 {
    let opponent_move = parse_move_1(moves.first().unwrap());
    let my_move = parse_intended_outcome(moves.last().unwrap(), opponent_move);
    let score = get_move_score(my_move) + calculate_score(opponent_move, my_move);
    return score;
}

fn parse_intended_outcome(outcome: &str, opponent_move: Moves) -> Moves {
    match outcome {
        "X" => wins_to(opponent_move),
        "Y" => opponent_move,
        "Z" => loses_to(opponent_move),
        &_ => todo!(),
    }
}

fn loses_to(_move: Moves) -> Moves {
    match _move {
        Moves::Paper => Moves::Scissor,
        Moves::Scissor => Moves::Rock,
        Moves::Rock => Moves::Paper,
    }
}

fn wins_to(_move: Moves) -> Moves {
    match _move {
        Moves::Paper => Moves::Rock,
        Moves::Scissor => Moves::Paper,
        Moves::Rock => Moves::Scissor,
    }
}

fn handle_round_first(moves: Vec<&str>) -> i32 {
    let opponent_move = parse_move_1(moves.first().unwrap());
    let my_move = parse_move_1(moves.last().unwrap());
    get_move_score(my_move) + calculate_score(opponent_move, my_move)
}

fn get_move_score(my_move: Moves) -> i32 {
    match my_move {
        Moves::Rock => ROCKSCORE,
        Moves::Paper => PAPERSCORE,
        Moves::Scissor => SCISSORSCORE,
    }
}

fn calculate_score(opponent_move: Moves, my_move: Moves) -> i32 {
    match opponent_move {
        Moves::Rock => match my_move {
            Moves::Paper => 6,
            Moves::Rock => 3,
            Moves::Scissor => 0,
        },
        Moves::Paper => match my_move {
            Moves::Paper => 3,
            Moves::Rock => 0,
            Moves::Scissor => 6,
        },
        Moves::Scissor => match my_move {
            Moves::Paper => 0,
            Moves::Rock => 6,
            Moves::Scissor => 3,
        },
    }
}

fn parse_move_1(_move: &str) -> Moves {
    match _move {
        "A" => Moves::Rock,
        "B" => Moves::Paper,
        "C" => Moves::Scissor,
        "X" => Moves::Rock,
        "Y" => Moves::Paper,
        "Z" => Moves::Scissor,
        &_ => todo!(),
    }
}
