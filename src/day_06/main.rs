use crate::utils::file::read_lines;

pub fn run() {
    let content = read_lines("resources/day6.txt")
        .first()
        .unwrap()
        .chars()
        .collect::<Vec<char>>();
    println!("Day 06:");
    let res1 = common(&content, 4);
    println!("{:?}", res1);
    let res2 = common(&content, 14);
    println!("{:?}", res2);
    println!("");
}

fn common(content: &Vec<char>, window_size: usize) -> usize {
    let windows = content.windows(window_size);
    for (i, window) in windows.enumerate() {
        let mut local_window = window.to_owned().clone();
        local_window.sort();
        local_window.dedup();
        if window.len() == local_window.len() {
            return i + window_size;
        }
    }
    return 0;
}

#[test]
fn test_first() {
    let content1 = "bvwbjplbgvbhsrlpgdmjqwftvncz"
        .chars()
        .collect::<Vec<char>>();
    let result1 = common(&content1, 4);
    assert_eq!(5, result1);
}

#[test]
fn test_first1() {
    let content2 = "nppdvjthqldpwncqszvftbrmjlhg"
        .chars()
        .collect::<Vec<char>>();
    let result2 = common(&content2, 4);
    assert_eq!(6, result2);
}

#[test]
fn test_first2() {
    let content3 = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
        .chars()
        .collect::<Vec<char>>();
    let result3 = common(&content3, 4);
    assert_eq!(10, result3);
}

#[test]
fn test_first3() {
    let content4 = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"
        .chars()
        .collect::<Vec<char>>();

    let result4 = common(&content4, 4);

    assert_eq!(11, result4);
}

#[test]
fn test_second() {
    let content1 = "bvwbjplbgvbhsrlpgdmjqwftvncz"
        .chars()
        .collect::<Vec<char>>();
    let result1 = common(&content1, 14);
    assert_eq!(23, result1);
}

#[test]
fn test_second1() {
    let content2 = "nppdvjthqldpwncqszvftbrmjlhg"
        .chars()
        .collect::<Vec<char>>();
    let result2 = common(&content2, 14);
    assert_eq!(23, result2);
}

#[test]
fn test_second2() {
    let content3 = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
        .chars()
        .collect::<Vec<char>>();
    let result3 = common(&content3, 14);
    assert_eq!(29, result3);
}

#[test]
fn test_second3() {
    let content4 = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"
        .chars()
        .collect::<Vec<char>>();

    let result4 = common(&content4, 14);

    assert_eq!(26, result4);
}
